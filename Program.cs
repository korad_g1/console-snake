﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gígyó
{
    class Program
    {
        static void Main(string[] args)
        {
            bool kilep = false;
          
            do
            {
                bool jatekFut = true;
                bool falnakment = false;
                bool megette = false;
                Console.CursorVisible = false;
                int AlmaX = 10;
                int AlmaY = 10;
                int Almaszamol = 0;
                Random r = new Random();

                decimal Kigyosebesseg = 120m;

                // kígyó kirajzol

                int[] xpoz = new int[100];
                xpoz[0] = 35;
                int[] ypoz = new int[100];
                ypoz[0] = 20;

                Console.SetCursorPosition(xpoz[0], ypoz[0]);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine((char)214);

                AlmaSpawner(r, out AlmaX, out AlmaY);
                //-----------
                //határ

                Fal();

                //-----
                //kígyó nmozog

                ConsoleKey parancs = Console.ReadKey().Key;

                do
                {
                    switch (parancs)
                    {
                        case ConsoleKey.LeftArrow:
                            Console.SetCursorPosition(xpoz[0], ypoz[0]);
                            Console.Write(" ");
                            xpoz[0]--;
                            break;

                        case ConsoleKey.RightArrow:
                            Console.SetCursorPosition(xpoz[0], ypoz[0]);
                            Console.Write(" ");
                            xpoz[0]++;
                            break;

                        case ConsoleKey.UpArrow:
                            Console.SetCursorPosition(xpoz[0], ypoz[0]);
                            Console.Write(" ");
                            ypoz[0]--;
                            break;

                        case ConsoleKey.DownArrow:
                            Console.SetCursorPosition(xpoz[0], ypoz[0]);
                            Console.Write(" ");
                            ypoz[0]++;
                            break;
                    }




                    //Spawn alma
                    if (megette)
                    {
                        AlmaSpawner(r, out AlmaX, out AlmaY);
                        Kigyosebesseg *= .925m;
                        Almaszamol++;
                        Console.Beep();
                    }


                    //-------
                    //Alma evés
                    megette = MegetteazAlmat(xpoz[0], ypoz[0], AlmaX, AlmaY);

                    //---------
                    //Kígyó kirajzol
                    KigyoRajzolo(Almaszamol, xpoz, ypoz, out xpoz, out ypoz);
                    //--------------

                    //kígyó nekimegy a falnak + játék lassítás

                    falnakment = NekimentaFalnak(xpoz[0], ypoz[0]);
                    if (falnakment)
                    {
                        jatekFut = false;
                        Console.SetCursorPosition(28, 20);
                        Console.WriteLine("Meghaltál");
                        Console.SetCursorPosition(28, 22);
                        Console.WriteLine("Pontszám: {0}", Almaszamol);
                        kilep=ujjatek();
                    }


                    if (Console.KeyAvailable) parancs = Console.ReadKey().Key;
                    System.Threading.Thread.Sleep(Convert.ToInt32(Kigyosebesseg));

                    //------------

                } while (jatekFut);

                //--------
            }
            while (kilep == false);

            
            Console.Read();

        }

        private static bool ujjatek()
        {
            Console.SetCursorPosition(28, 24);
            Console.WriteLine("Új játék? y/n");
            Console.SetCursorPosition(28, 28);
            string x = Console.ReadLine();
            x = x.ToLower();
            if (x == "y")
            {
                Console.Clear();
                Fal();
                return false;
            }
            else
            {
                return true;
            }
        }
        private static void KigyoRajzolo(int almaszamol, int[] xpozbe, int[] ypozbe, out int[] xpozki, out int[] ypozki)
        {
            //fej
            Console.SetCursorPosition(xpozbe[0], ypozbe[0]);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine((char)214);

            //test
            for (int i = 1; i < almaszamol+1; i++)
            {
                Console.SetCursorPosition(xpozbe[i], ypozbe[i]);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("o");
            }

            //mozdul a kígyó
            Console.SetCursorPosition(xpozbe[almaszamol+1], ypozbe[almaszamol+1]);
            Console.WriteLine(" ");

            //test darabok helye
            for (int i = almaszamol+1; i > 0; i--)
            {
                xpozbe[i] = xpozbe[i - 1];
                ypozbe[i] = ypozbe[i - 1];
            }

            xpozki = xpozbe;
            ypozki = ypozbe;
        }

        private static bool MegetteazAlmat(int xpoz, int ypoz, int almaX, int almaY)
        {
            if (xpoz == almaX && ypoz == almaY) return true; return false;
        }

        private static void AlmaSpawner(Random r, out int almaX, out int almaY)
        {
            almaX = r.Next(0 + 2, 70 - 2);
            almaY = r.Next(0 + 2, 40 - 2);


            Console.SetCursorPosition(almaX, almaY);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write((char)64);
        }

        private static bool NekimentaFalnak(int xpoz, int ypoz)
        {
            if (xpoz == 1 || xpoz == 70 || ypoz == 1 || ypoz == 40) return true; return false;
        }

        private static void Fal()
        {
            for (int i = 1; i < 41; i++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.SetCursorPosition(1, i);
                Console.Write("#");
                Console.SetCursorPosition(70, i);
                Console.Write("#");
            }
            for (int i = 1; i < 71; i++)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.SetCursorPosition(i, 1);
                Console.Write("#");
                Console.SetCursorPosition(i,40);
                Console.Write("#");
            }
        }
    }
}
